<img width="660" height="200" src="banner.png">

---

### Tested for Multiplayer! All players must have the mods installed and enabled.

---

### Last Updated - 11/23/2020

### Latest Version Tested - 12.01

### Current Mod Version - 0.10.0b

---

## Summary
This mod attempts to expand on existing items and tools that exist as part of Raft while keeping them as moderately close as possible to Vanilla. Unlike Augmented Equipment, this mod provides on items that don't rely on some sort of hidden modifier that improves on Player properties. Instead, the item already contains all the properties that you would expect from standard crafts and upgrades. However, if desired, this mod can be used side-by-side with [Augmented Equipment](https://www.raftmodding.com/mods/augmented-equipment) to further the feeling of power-creep.

Initially, the mod will contain low quantity of items but over time, you can expect more to be added.

---

## Augments

| Item Icon | Item Name | Craft Type | Durability | Item Improvement |
| :---: | :---: | :---: | :---: | :---: |
| <img width="64" height="64" src="Assets/Hook_Titanium.png"> | Titanium Hook | Tool | 500 | 0.5s Gather Time |
| <img width="64" height="64" src="Assets/Titanium_Shovel.png"> | Titanium Shovel | Tool | 120 | +50% Dig Speed |
| <img width="64" height="64" src="Assets/Titanium_Axe.png"> | Titanium Axe | Tool | 300 | 45% Faster Swing Speed |
| <img width="64" height="64" src="Assets/Aluminum_Bow.png"> | Aluminum Bow | Weapon | 110 | +40% Draw Speed |
| <img width="64" height="64" src="Assets/Titanium_Arrow.png"> | Titanium Arrow | Weapon | 1 | 20 Damage |
| <img width="64" height="64" src="Assets/Placeable_Storage_MediumTitanium.png"> | Titanium Storage | Storages | 1 | Large Storage in size of regular Storage, Wall-placeable |
| <img width="64" height="64" src="Assets/Placeable_MotorWheel_Titanium.png"> | Titanium Engine | Navigation | 1 | +75% Faster / +33% Fuel Efficiency / 200 Foundations |
| <img width="64" height="64" src="Assets/Placeable_CookingStand_SmelterArc.png"> | Arc Furnace | Others | 1 | +50% Smelting Speed |

---

## Contact and Issues
Please contact me on Discord - Bahamut#2632 or the [Raft Modding Discord Server](https://discord.com/invite/Dwz7GSA)!