﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using static AugmentedEquipmentItems;
using static CustomHook;
using static CustomShovel;
using static CustomBow;
using static CustomArrow;
using static CustomAxe;
using static CustomChest;
using static CustomEngine;
using static CustomSmelter;
using HarmonyLib;
using Steamworks;
using UnityEngine;

public class AugmentedItems : Mod {
  private Harmony harmony;
  private string instanceId = "com.bahamut.augmenteditems";
  private static AssetBundle asset;

  private static List<Item_Base> addedItems = new List<Item_Base>();
  private static Animator cachedLocalAnimator = null;
  private static Dictionary<string, ItemConnection> addedConnectionDictionary = new Dictionary<string, ItemConnection>();
  private static List<AnimationConnection> addedAnimationConnectionList = new List<AnimationConnection>();
  private static Dictionary<string, GameObject> rightHandGameObjects = new Dictionary<string, GameObject> {
    ["hook"] = null
  };
  private static Dictionary<string, AnimationConnection> animationConnectionTemplates = new Dictionary<string, AnimationConnection> {
    ["hookThrow"] = null,
    ["hookGather"] = null
  };

  public IEnumerator Start() {
    AssetBundleCreateRequest request = AssetBundle.LoadFromMemoryAsync(GetEmbeddedFileBytes("com.bahamut.augmenteditems.assets"));
    yield return request;
    asset = request.assetBundle;

    LoadAugmentedItems();
    LoadAugmentedPlaceables();

    harmony = new Harmony(instanceId);
    harmony.PatchAll(Assembly.GetExecutingAssembly());
    Debug.Log("Mod AugmentedItems has been loaded!");
  }

  public void OnModUnload() {
    harmony.UnpatchAll(instanceId);
    UnloadItems();
    UnloadAllInstantiatedObjects();
    asset.Unload(true);
    Debug.Log("Mod AugmentedItems has been unloaded!");
  }

  // Helper Methods
  private static void LoadRightHandComponents(Network_Player networkPlayer) {
    Transform rightHandTransform = networkPlayer.rightHandParent;
    foreach (Transform rightHandItemTransform in rightHandTransform) {
      GameObject rightHandGameObject = rightHandItemTransform.gameObject;
      if (rightHandGameObjects["hook"] == null) {
        Component hookScript = rightHandGameObject.GetComponent(typeof(Hook));
        if (hookScript != null) {
          rightHandGameObjects["hook"] = Instantiate(rightHandGameObject);
          rightHandGameObjects["hook"].SetActive(false);
        }
      }
    }
  }

  private static void CreateAnimationConnectionTemplates(Network_Player networkPlayer, List<AnimationConnection> animationConnections) {
    AnimationConnection connectionTemplate = null;
    foreach (AnimationConnection connection in animationConnections) {
      if (connection.name.Equals("Hook_Throw | Animation_HookThrow")) {
        connectionTemplate = new AnimationConnection();
        CopyAnimationConnection(connectionTemplate, connection);
        animationConnectionTemplates["hookThrow"] = connectionTemplate;
      }
      if (connection.name.Equals("Gather | OnHookGather")) {
        connectionTemplate = new AnimationConnection();
        CopyAnimationConnection(connectionTemplate, connection);
        animationConnectionTemplates["hookGather"] = connectionTemplate;
      }
    }
  }

  private static void CopyAnimationConnection(AnimationConnection connectionTemplate, AnimationConnection targetConnection) {
    connectionTemplate.name = targetConnection.name;
    connectionTemplate.layerName = targetConnection.layerName;
    connectionTemplate.stateName = targetConnection.stateName;
    connectionTemplate.functionName = targetConnection.functionName;
    connectionTemplate.functionParameter = targetConnection.functionParameter;
    connectionTemplate.timeOfEvent = targetConnection.timeOfEvent;
    connectionTemplate.active = targetConnection.active;
    connectionTemplate.calledEvent = targetConnection.calledEvent;
    connectionTemplate.lastNormalizedTime = targetConnection.lastNormalizedTime;
  }

  private static void WireUpComponents() {

  }

  private void LoadAugmentedItems() {
    foreach(KeyValuePair<string, ItemTemplate> kvp in AugmentedEquipmentItems.AllAugmentedEquipmentItems) {
      ItemTemplate itemTemplate = kvp.Value;
      if (itemTemplate.uniqueName != null && !itemTemplate.uniqueName.Contains("Placeable_")) {
        CreateEquipment(
          itemTemplate.baseItemReference,
          itemTemplate.uniqueName,
          itemTemplate.displayName,
          itemTemplate.uniqueIndex,
          itemTemplate.description,
          itemTemplate.maxUses,
          itemTemplate.equipSlotType,
          itemTemplate.craftingCategory,
          itemTemplate.subCategory,
          itemTemplate.subCategoryOrder,
          itemTemplate.recipe,
          itemTemplate.spriteName
        );
      }
    }
  }

  private void LoadAugmentedPlaceables() {
    foreach(KeyValuePair<string, ItemTemplate> kvp in AugmentedEquipmentItems.AllAugmentedEquipmentItems) {
      ItemTemplate itemTemplate = kvp.Value;
      if (itemTemplate.uniqueName != null && itemTemplate.uniqueName.Contains("Placeable_")) {
        CreatePlaceable(
          itemTemplate.baseItemReference,
          itemTemplate.uniqueName,
          itemTemplate.displayName,
          itemTemplate.uniqueIndex,
          itemTemplate.description,
          itemTemplate.maxUses,
          itemTemplate.equipSlotType,
          itemTemplate.craftingCategory,
          itemTemplate.subCategory,
          itemTemplate.subCategoryOrder,
          itemTemplate.recipe,
          itemTemplate.spriteName
        );
      }
    }
  }

  private Item_Base GenerateItemBase(string baseItemName, string uniqueName, string displayName, int uniqueIndex, string description, int maxUses, EquipSlotType slotType, CraftingCategory craftingCategory, string subCategory, int subCategoryOrder, Dictionary<string, int> recipe, string spriteName) {
    string spriteAsset = (spriteName != null) ? spriteName : uniqueName;
    Sprite augmentedItemSprite = asset.LoadAsset<Sprite>(spriteAsset);
    Item_Base baseEquipmentItem = ItemManager.GetItemByName(baseItemName);
    Item_Base newEquipmentItem = UnityEngine.Object.Instantiate(baseEquipmentItem);

    // Update the properties the item -- overriding the initialized property so we can set maxUses.
    if (baseItemName.Equals("Backpack")) {
      Traverse.Create(newEquipmentItem).Property("UniqueName").SetValue(uniqueName);
      Traverse.Create(newEquipmentItem).Property("UniqueIndex").SetValue(uniqueIndex);
      ItemInstance_Inventory itemInstanceInventory = new ItemInstance_Inventory(null, "", 1);
      Traverse.Create(newEquipmentItem).Field("settings_Inventory").SetValue(itemInstanceInventory);
    } else {
      Traverse.Create(newEquipmentItem).Field("hasBeenInitialized").SetValue(false);
      newEquipmentItem.Initialize(uniqueIndex, uniqueName, maxUses);
    }
    Traverse.Create(newEquipmentItem).Field("settings_Inventory").Property("DisplayName").SetValue(displayName);
    Traverse.Create(newEquipmentItem).Field("settings_Inventory").Property("Description").SetValue(description);
    Traverse.Create(newEquipmentItem).Field("settings_Inventory").Property("Sprite").SetValue(augmentedItemSprite);
    ItemInstance_Equipment equipmentSlotType = new ItemInstance_Equipment(slotType);
    Traverse.Create(newEquipmentItem).Field("settings_equipment").SetValue(equipmentSlotType);

    ItemInstance_Recipe itemInstanceRecipe = new ItemInstance_Recipe(craftingCategory, false, false, subCategory, subCategoryOrder);
    newEquipmentItem.settings_recipe = itemInstanceRecipe;

    return newEquipmentItem;
  }

  private void CreateEquipment(string baseItemName, string uniqueName, string displayName, int uniqueIndex, string description, int maxUses, EquipSlotType slotType, CraftingCategory craftingCategory, string subCategory, int subCategoryOrder, Dictionary<string, int> recipe, string spriteName = null) {
    Item_Base newEquipmentItem = GenerateItemBase(baseItemName, uniqueName, displayName, uniqueIndex, description, maxUses, slotType, craftingCategory, subCategory, subCategoryOrder, recipe, spriteName);

    // Create the recipe for the item.
    List<CostMultiple> itemRecipe = new List<CostMultiple>();
    foreach (KeyValuePair<string, int> kvp in recipe) {
      Item_Base item = ItemManager.GetItemByName(kvp.Key);
      CostMultiple itemRecipeQuantity = new CostMultiple(new Item_Base[] { item }, kvp.Value);
      itemRecipe.Add(itemRecipeQuantity);
    }
    newEquipmentItem.settings_recipe.NewCost = itemRecipe.ToArray();
    if (uniqueName.Contains("_Arrow")) {
      Traverse.Create(newEquipmentItem.settings_recipe).Field("amountToCraft").SetValue(6);
    }
    addedItems.Add(newEquipmentItem);

    // Debug.Log($"[Item] Successfully Added {newEquipmentItem.UniqueName}");
    RAPI.RegisterItem(newEquipmentItem);
  }

  private void CreatePlaceable(string baseItemName, string uniqueName, string displayName, int uniqueIndex, string description, int maxUses, EquipSlotType slotType, CraftingCategory craftingCategory, string subCategory, int subCategoryOrder, Dictionary<string, int> recipe, string spriteName = null) {
    CustomEngine customEngineTemplate = new CustomEngine(asset);
    CustomSmelter customSmelterTemplate = new CustomSmelter(asset);

    Item_Base newEquipmentItem = null;
    if (uniqueName.Contains("Placeable_Storage_")) {
      // For some reason, we're not able to cleanly programmatically create new Placeables...
      // Using this as a temporary work-around while I dabble with the code a bit more.
      newEquipmentItem = asset.LoadAsset<Item_Base>(uniqueName);
      RAPI.AddItemToBlockQuadType(newEquipmentItem, RBlockQuadType.quad_floor);
      RAPI.AddItemToBlockQuadType(newEquipmentItem, RBlockQuadType.quad_foundation);
      RAPI.AddItemToBlockQuadType(newEquipmentItem, RBlockQuadType.quad_wall);
    } else {
      newEquipmentItem = GenerateItemBase(baseItemName, uniqueName, displayName, uniqueIndex, description, maxUses, slotType, craftingCategory, subCategory, subCategoryOrder, recipe, spriteName);
    }

    if (uniqueName.Contains("Placeable_MotorWheel_")) {
      customEngineTemplate.Create(newEquipmentItem);
    }

    if (uniqueName.Contains("Placeable_CookingStand_")) {
      customSmelterTemplate.Create(newEquipmentItem);
    }

    // Create the recipe for the item.
    List<CostMultiple> itemRecipe = new List<CostMultiple>();
    foreach (KeyValuePair<string, int> kvp in recipe) {
      Item_Base item = ItemManager.GetItemByName(kvp.Key);
      CostMultiple itemRecipeQuantity = new CostMultiple(new Item_Base[] { item }, kvp.Value);
      itemRecipe.Add(itemRecipeQuantity);
    }
    newEquipmentItem.settings_recipe.NewCost = itemRecipe.ToArray();
    if (uniqueName.Contains("_Arrow")) {
      Traverse.Create(newEquipmentItem.settings_recipe).Field("amountToCraft").SetValue(6);
    }
    addedItems.Add(newEquipmentItem);

    // Debug.Log($"[Placeable] Successfully Added {newEquipmentItem.UniqueName}");
    RAPI.RegisterItem(newEquipmentItem);
  }

  private void UnloadItems() {
    List<Item_Base> allItems = Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").GetValue() as List<Item_Base>;
    foreach (Item_Base item in addedItems) {
      // Debug.Log($"Unloading Item {item.UniqueName}...");
      allItems.Remove(item);
    }
    addedItems.Clear();
    Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").SetValue(allItems);
  }

  private static void UnloadAllInstantiatedObjects() {
    List<string> rightHandGameObjectsKeys = new List<string>(rightHandGameObjects.Keys);
    foreach (string objectKey in rightHandGameObjectsKeys) {
      if (rightHandGameObjects[objectKey] != null) {
        Destroy(rightHandGameObjects[objectKey]);
        rightHandGameObjects[objectKey] = null;
      }
    }
  }

  private static void UnloadAllAddedConnections(Network_Player networkPlayer) {
    Dictionary<string, ItemConnection> connectionDictionary = Traverse.Create(networkPlayer.PlayerItemManager.useItemController).Field("connectionDictionary").GetValue() as Dictionary<string, ItemConnection>;
    GameObject playerAnimatorGameObject = networkPlayer.Animator.gameObject;
    AnimationEventCaller animationEventCaller = playerAnimatorGameObject.GetComponent<AnimationEventCaller>();
    List<AnimationConnection> animationConnections = Traverse.Create(animationEventCaller).Field("connections").GetValue() as List<AnimationConnection>;
    
    // Remove the MonoBehaviour_Network from the NetworkUpdateManager as well before closing out as a local player as well.
    foreach (KeyValuePair<string, ItemConnection> kvp in addedConnectionDictionary) {
      GameObject connectionItemGameObject = kvp.Value.obj;
      MonoBehaviour_ID_Network itemIdNetwork = connectionItemGameObject.GetComponent<MonoBehaviour_ID_Network>();
      if (itemIdNetwork is MonoBehaviour_Network) {
        MonoBehaviour_Network itemNetwork = itemIdNetwork as MonoBehaviour_Network;
        NetworkUpdateManager.RemoveBehaviour(itemNetwork);
      } else {
        if (itemIdNetwork != null) {
          NetworkIDManager.RemoveNetworkID(itemIdNetwork);
        }
      }
    }

    foreach (KeyValuePair<string, ItemConnection> kvp in addedConnectionDictionary) {
      connectionDictionary.Remove(kvp.Key);
    }

    foreach (AnimationConnection animationConnection in addedAnimationConnectionList) {
      animationConnections.Remove(animationConnection);
    }

    addedConnectionDictionary.Clear();
    addedAnimationConnectionList.Clear();
  }

  public override void WorldEvent_WorldLoaded() {
    Raft raft = ComponentManager<Raft>.Value;
    if (raft != null)
    {
        raft.maxSpeed = 11f;
        raft.maxVelocity = 11f;
    }
  }

  // Patches
  [HarmonyPatch(typeof(Network_Player), "InitializeComponents")]
  class Network_PlayerInitializeComponents_Patch {
    private static void Postfix(Network_Player __instance) {
      Network_Player networkPlayer = __instance;
      Dictionary<string, ItemConnection> connectionDictionary = Traverse.Create(networkPlayer.PlayerItemManager.useItemController).Field("connectionDictionary").GetValue() as Dictionary<string, ItemConnection>;

      GameObject playerAnimatorGameObject = networkPlayer.Animator.gameObject;
      AnimationEventCaller animationEventCaller = playerAnimatorGameObject.GetComponent<AnimationEventCaller>();
      List<AnimationConnection> animationConnections = Traverse.Create(animationEventCaller).Field("connections").GetValue() as List<AnimationConnection>;

      Transform playerRightHandTransform = networkPlayer.rightHandParent;

      CustomHook customHookTemplate = new CustomHook(playerRightHandTransform, connectionDictionary, addedConnectionDictionary, animationConnections, addedAnimationConnectionList, asset);
      CustomShovel customShovelTemplate = new CustomShovel(playerRightHandTransform, connectionDictionary, addedConnectionDictionary, animationConnections, addedAnimationConnectionList, asset);
      CustomBow customBowTemplate = new CustomBow(playerRightHandTransform, connectionDictionary, addedConnectionDictionary, animationConnections, addedAnimationConnectionList, asset);
      CustomArrow customArrowTemplate = new CustomArrow(playerRightHandTransform, connectionDictionary, addedConnectionDictionary, animationConnections, addedAnimationConnectionList, asset);
      CustomAxe customAxeTemplate = new CustomAxe(playerRightHandTransform, connectionDictionary, addedConnectionDictionary, animationConnections, addedAnimationConnectionList, asset);

      foreach (Item_Base itemBase in addedItems) {
        if (itemBase.UniqueName.Contains("Hook")) {
          customHookTemplate.Create(itemBase);
        }

        if (itemBase.UniqueName.Contains("_Shovel")) {
          customShovelTemplate.Create(itemBase);
        }

        if (itemBase.UniqueName.Contains("_Bow")) {
          customBowTemplate.Create(itemBase);
        }

        if (itemBase.UniqueName.Contains("_Axe")) {
          customAxeTemplate.Create(itemBase);
        }
      }

      foreach (Item_Base itemBase in addedItems) {
        if (itemBase.UniqueName.Contains("_Arrow")) {
          customArrowTemplate.Create(itemBase);
        }
      }

      // Should no longer need this due to all animations and connections of the custom items being front loaded.
      // // Invoke the SetIndexLocalPlayer code based on the original conditions of the Raft code.
      // if (Semih_Network.IsHost && (GameManager.IsInNewGame || !__instance.IsLocalPlayer)) {
      //   __instance.SetIndexLocalPlayer();
      // }

      // networkPlayer.GetComponentInChildren<PlayerFOVManager>().Initialize();
      // RAPI.GetLocalPlayer().GetComponentInChildren<PlayerFOVManager>().Initialize();
      // Debug.Log("-- After Edits - Network Player: " + __instance);
    }
  }

  [HarmonyPatch(typeof(Player), "SceneEventInterface.OnSceneEvent", new[] { typeof(SceneEvent) })]
  class PlayerOnSceneEvent_Patch {
    private static void Prefix(SceneEvent sceneEvent) {
      if (sceneEvent == SceneEvent.LeaveGame) {
        Network_Player networkPlayer = RAPI.GetLocalPlayer();
        if (networkPlayer.IsLocalPlayer) {
          UnloadAllAddedConnections(networkPlayer);
        }
      }
    }
  }

  [HarmonyPatch(typeof(Hotbar), "SelectHotslot", new[] { typeof(Slot) })]
  class HotbarSelectHotslot_Patch {
    private static void Postfix(Hotbar __instance, Slot slot) {
      if (!__instance.playerNetwork.IsLocalPlayer) {
        return;
      }
      if (slot == null || slot.IsEmpty) {
        return;
      }
      if (cachedLocalAnimator == null) {
        PlayerAnimator playerAnimator = __instance.playerNetwork.Animator;
        cachedLocalAnimator = playerAnimator.anim;
      }

      ItemInstance itemInstance = slot.itemInstance;
      if (itemInstance.UniqueName.Contains("_Shovel")) { 
        cachedLocalAnimator.speed = 1f + (1f * AugmentedEquipmentItems.Get(itemInstance.UniqueName).modifiers["digSpeedModifier"]);
      } else {
        cachedLocalAnimator.speed = 1f;
      }
    }
  }

  [HarmonyPatch(typeof(Throwable_Object), "Initialize")]
  class Throwable_ObjectInitialize_Patch {
    private static void Prefix(Throwable_Object __instance) {
      __instance.gameObject.SetActive(true);
    }
  }
}
