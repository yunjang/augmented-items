# 0.10.0b
- Added in Custom Axe Support
- Added in Titanium Axe

# 0.9.0b
- Code refactored for cleaner and speedier development
- Added in Custom Cooking Stand Support
- Added in Arc Furnace

# 0.8.0b
- Added in Custom Engine Support
- Added in Titanium Engine

# 0.7.0b
- Added in Titanium Storage
- Updating mod to be permanent on load to avoid bugs from unloading

# 0.6.3b
- Fixed Host/Client Error and Gamelocking on Rejoins
  - By frontloading all custom item generation and insertions to `InitializeComponents` of the `Network_Player` rather than after `OnPlayerCreated`

# 0.6.2b
- Modinfo Update

# 0.6.1b
- RMOD File Size Reduction

# 0.6.0b
- Code Refactored
- Custom Arrow Support Added
- Titanium Arrow Added

# 0.5.0b
- Code Refactored
- Fixed Multiplayer Bug (Again, sorry.)
- Custom Bow Support Added
- Aluminum Bow Added

# 0.4.0b
- Custom Shovel Support Added
- Titanium Shovel Added

# 0.3.1b
- Fixed bug when unloading and reloading the mod

# 0.3.0b
- Fixed Multiplayer Issues*

# 0.2.0b
- Removal of caching of assets
  - While not the right solution, this allows the hook asset to be scene by Players in Multiplayer
    - However, this is still a buggy mess

# 0.1.0b
- Initial Commit of Mod