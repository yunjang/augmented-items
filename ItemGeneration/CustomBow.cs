using System.Collections;
using System.Collections.Generic;
using static AugmentedEquipmentItems;
using HarmonyLib;
using UnityEngine;

public class CustomBow : CustomItem {
  public CustomBow(Transform playerRightHandTransform, Dictionary<string, ItemConnection> connectionDictionary, Dictionary<string, ItemConnection> addedConnectionDictionary, List<AnimationConnection> animationConnections, List<AnimationConnection> addedAnimationConnectionList, AssetBundle asset) : base(playerRightHandTransform, connectionDictionary, addedConnectionDictionary, animationConnections, addedAnimationConnectionList, asset) { }

  public void Create(Item_Base itemBase) {
    GameObject bowTemplateGameObject = null;
    foreach (Transform rightHandItem in base.playerRightHandTransform) {
      ThrowableComponent_Bow templateBowScript = rightHandItem.gameObject.GetComponent<ThrowableComponent_Bow>();
      if (templateBowScript != null) {
        bowTemplateGameObject = rightHandItem.gameObject;
        break;
      }
    }

    GameObject newBowGameObject = GameObject.Instantiate(bowTemplateGameObject, base.playerRightHandTransform);
    newBowGameObject.SetActive(false);
    newBowGameObject.name = itemBase.UniqueName;
    newBowGameObject.transform.localPosition = Vector3.zero;

    ChargeMeter bowChargeMeter = newBowGameObject.GetComponent<ChargeMeter>();
    Traverse.Create(bowChargeMeter).Field("chargeSpeed").SetValue(75f + (75f * AugmentedEquipmentItems.Get(itemBase.UniqueName).modifiers["chargeSpeedModifier"]));

    foreach (Transform bowTransform in newBowGameObject.transform) {
      if (bowTransform.gameObject.name.Equals("bow animator")) {
        foreach (Transform bowModelTransform in bowTransform) {
          if (bowModelTransform.gameObject.name.Contains("model")) {
            GameObject bowModelObject = bowModelTransform.gameObject;
            Material importedBowMaterial = asset.LoadAsset<Material>(itemBase.UniqueName + "_Material");
            SkinnedMeshRenderer bowMeshRenderer = bowModelObject.GetComponent<SkinnedMeshRenderer>();
            bowMeshRenderer.material = importedBowMaterial;
          }
        }
      }
    }

    ItemConnection itemConnection = new ItemConnection();
    itemConnection.name = itemBase.UniqueName;
    itemConnection.inventoryItem = itemBase;
    itemConnection.obj = newBowGameObject;
    connectionDictionary.TryAdd(itemBase.UniqueName, itemConnection);
    addedConnectionDictionary.TryAdd(itemBase.UniqueName, itemConnection);
  }
}
