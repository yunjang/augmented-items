using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomItem {
  public Transform playerRightHandTransform;
  public Dictionary<string, ItemConnection> connectionDictionary;
  public Dictionary<string, ItemConnection> addedConnectionDictionary;
  public List<AnimationConnection> animationConnections;
  public List<AnimationConnection> addedAnimationConnectionList;
  public AssetBundle asset;

  public CustomItem(Transform playerRightHandTransform, Dictionary<string, ItemConnection> connectionDictionary, Dictionary<string, ItemConnection> addedConnectionDictionary, List<AnimationConnection> animationConnections, List<AnimationConnection> addedAnimationConnectionList, AssetBundle asset) {
    this.playerRightHandTransform = playerRightHandTransform;
    this.connectionDictionary = connectionDictionary;
    this.addedConnectionDictionary = addedConnectionDictionary;
    this.animationConnections = animationConnections;
    this.addedAnimationConnectionList = addedAnimationConnectionList;
    this.asset = asset;
  }

  public void CopyAnimationConnection(AnimationConnection connectionTemplate, AnimationConnection targetConnection) {
    connectionTemplate.name = targetConnection.name;
    connectionTemplate.layerName = targetConnection.layerName;
    connectionTemplate.stateName = targetConnection.stateName;
    connectionTemplate.functionName = targetConnection.functionName;
    connectionTemplate.functionParameter = targetConnection.functionParameter;
    connectionTemplate.timeOfEvent = targetConnection.timeOfEvent;
    connectionTemplate.active = targetConnection.active;
    connectionTemplate.calledEvent = targetConnection.calledEvent;
    connectionTemplate.lastNormalizedTime = targetConnection.lastNormalizedTime;
  }
}