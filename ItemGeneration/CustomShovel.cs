using System.Collections;
using System.Collections.Generic;
using static AugmentedEquipmentItems;
using UnityEngine;

public class CustomShovel : CustomItem {
  public CustomShovel(Transform playerRightHandTransform, Dictionary<string, ItemConnection> connectionDictionary, Dictionary<string, ItemConnection> addedConnectionDictionary, List<AnimationConnection> animationConnections, List<AnimationConnection> addedAnimationConnectionList, AssetBundle asset) : base(playerRightHandTransform, connectionDictionary, addedConnectionDictionary, animationConnections, addedAnimationConnectionList, asset) { }

  public void Create(Item_Base itemBase) {
    GameObject shovelTemplateGameObject = null;
    foreach (Transform rightHandItem in base.playerRightHandTransform) {
      Shovel templateShovelScript = rightHandItem.gameObject.GetComponent<Shovel>();
      if (templateShovelScript != null) {
        shovelTemplateGameObject = rightHandItem.gameObject;
        break;
      }
    }

    AnimationConnection digDownAnimation = null;
    AnimationConnection digThrowAnimation = null;
    foreach (AnimationConnection connection in base.animationConnections) {
      if (connection.name.Equals("Shovel_Use | DigDown") && digDownAnimation == null) {
        digDownAnimation = new AnimationConnection();
        base.CopyAnimationConnection(digDownAnimation, connection);
      }
      if (connection.name.Equals("Shovel_Use | DigThrow") && digThrowAnimation == null) {
        digThrowAnimation = new AnimationConnection();
        base.CopyAnimationConnection(digThrowAnimation, connection);
      }
    }

    GameObject newShovelGameObject = GameObject.Instantiate(shovelTemplateGameObject, base.playerRightHandTransform);
    newShovelGameObject.SetActive(false);
    newShovelGameObject.name = itemBase.UniqueName;
    newShovelGameObject.transform.localPosition = Vector3.zero;

    foreach (Transform shovelModelTransform in newShovelGameObject.transform) {
      if (shovelModelTransform.gameObject.name.Contains("Model")) {
        GameObject shovelModelObject = shovelModelTransform.gameObject;
        Material importedShovelMaterial = asset.LoadAsset<Material>(itemBase.UniqueName + "_Material");
        MeshRenderer shovelMeshRenderer = shovelModelObject.GetComponent<MeshRenderer>();
        shovelMeshRenderer.material = importedShovelMaterial;
      }
    }

    ItemConnection itemConnection = new ItemConnection();
    itemConnection.name = itemBase.UniqueName;
    itemConnection.inventoryItem = itemBase;
    itemConnection.obj = newShovelGameObject;
    connectionDictionary.TryAdd(itemBase.UniqueName, itemConnection);
    addedConnectionDictionary.TryAdd(itemBase.UniqueName, itemConnection);

    digDownAnimation.objectReciever = newShovelGameObject;
    animationConnections.Add(digDownAnimation);
    addedAnimationConnectionList.Add(digDownAnimation);

    digThrowAnimation.objectReciever = newShovelGameObject;
    animationConnections.Add(digThrowAnimation);
    addedAnimationConnectionList.Add(digThrowAnimation);
  }
}
