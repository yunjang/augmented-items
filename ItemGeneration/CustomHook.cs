using System.Collections;
using System.Collections.Generic;
using static AugmentedEquipmentItems;
using UnityEngine;

// @TODO: Singular function does way too much. Isolate and make functions return rather than update through reference.
public class CustomHook : CustomItem {
  public CustomHook(Transform playerRightHandTransform, Dictionary<string, ItemConnection> connectionDictionary, Dictionary<string, ItemConnection> addedConnectionDictionary, List<AnimationConnection> animationConnections, List<AnimationConnection> addedAnimationConnectionList, AssetBundle asset) : base(playerRightHandTransform, connectionDictionary, addedConnectionDictionary, animationConnections, addedAnimationConnectionList, asset) { }

  public void Create(Item_Base itemBase) {
    GameObject hookTemplateGameObject = null;
    foreach (Transform rightHandItem in base.playerRightHandTransform) {
      Hook templateHookScript = rightHandItem.gameObject.GetComponent<Hook>();
      if (templateHookScript != null) {
        hookTemplateGameObject = rightHandItem.gameObject;
        break;
      }
    }

    AnimationConnection hookThrowAnimation = null;
    AnimationConnection hookGatherAnimation = null;
    foreach (AnimationConnection connection in base.animationConnections) {
      if (connection.name.Equals("Hook_Throw | Animation_HookThrow") && hookThrowAnimation == null) {
        hookThrowAnimation = new AnimationConnection();
        base.CopyAnimationConnection(hookThrowAnimation, connection);
      }
      if (connection.name.Equals("Gather | OnHookGather") && hookGatherAnimation == null) {
        hookGatherAnimation = new AnimationConnection();
        base.CopyAnimationConnection(hookGatherAnimation, connection);
      }
    }

    GameObject newHookGameObject = GameObject.Instantiate(hookTemplateGameObject, base.playerRightHandTransform);
    newHookGameObject.SetActive(false);
    newHookGameObject.name = itemBase.UniqueName;
    newHookGameObject.transform.localPosition = Vector3.zero;

    Hook hookScript = newHookGameObject.GetComponent<Hook>();
    hookScript.gatherTime = AugmentedEquipmentItems.AllAugmentedEquipmentItems[itemBase.UniqueName].modifiers["gatherTime"];

    foreach (Transform hookTransform in newHookGameObject.transform) {
      foreach (Transform hookModelTransform in hookTransform) {
        if (hookModelTransform.gameObject.name.Contains("Model")) {
          GameObject hookModelObject = hookModelTransform.gameObject;
          Mesh importedHookMesh = asset.LoadAsset<Mesh>(itemBase.UniqueName);
          Material importedHookMaterial = asset.LoadAsset<Material>(itemBase.UniqueName + "_Material");
          MeshFilter hookMeshFilter = hookModelObject.GetComponent<MeshFilter>();
          MeshRenderer hookMeshRenderer = hookModelObject.GetComponent<MeshRenderer>();
          hookMeshFilter.mesh = importedHookMesh;
          hookMeshRenderer.material = importedHookMaterial;
        }
      }
    }

    ItemConnection itemConnection = new ItemConnection();
    itemConnection.name = itemBase.UniqueName;
    itemConnection.inventoryItem = itemBase;
    itemConnection.obj = newHookGameObject;
    connectionDictionary.TryAdd(itemBase.UniqueName, itemConnection);
    addedConnectionDictionary.TryAdd(itemBase.UniqueName, itemConnection);

    hookThrowAnimation.objectReciever = newHookGameObject;
    animationConnections.Add(hookThrowAnimation);
    addedAnimationConnectionList.Add(hookThrowAnimation);

    hookGatherAnimation.objectReciever = newHookGameObject;
    animationConnections.Add(hookGatherAnimation);
    addedAnimationConnectionList.Add(hookGatherAnimation);
  }
}
