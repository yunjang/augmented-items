using System;
using System.Collections;
using System.Collections.Generic;
using static AugmentedEquipmentItems;
using HarmonyLib;
using FMODUnity;
using UnityEngine;

public class CustomArrow : CustomItem {
  public CustomArrow(Transform playerRightHandTransform, Dictionary<string, ItemConnection> connectionDictionary, Dictionary<string, ItemConnection> addedConnectionDictionary, List<AnimationConnection> animationConnections, List<AnimationConnection> addedAnimationConnectionList, AssetBundle asset) : base(playerRightHandTransform, connectionDictionary, addedConnectionDictionary, animationConnections, addedAnimationConnectionList, asset) { }

  public void Create(Item_Base itemBase) {
    GameObject newArrowPrefab = null;
    foreach (Transform rightHandItem in playerRightHandTransform) {
      ThrowableComponent_Bow bowScript = rightHandItem.gameObject.GetComponent<ThrowableComponent_Bow>();
      if (bowScript != null) {
        // @TODO: Try and resolve the issue where the newly created arrows (model references) end up laying flat or 90 degrees only.
        // I have a feeling this may be tied to the way the models are being instantiated from the reference object.

        /*
         For each bow, we will do the following:
         - Grab the throwablePrefabs and throwableModelEnabler properties from the bow's throwable script.
         - Update StringMiddle with a copy of the metal arrow model, rename it, set it to inactive, store the reference.
         - Create the new arrow game object using an existing one (if it hasn't already been created).
         - Update throwablePrefabs with the new arrow game object.
         - Update throwableModelEnabler with the reference to the copy of the metal arrow model.
        */
        GameObject bowGameObject = bowScript.gameObject;
        // For now, we're using the same material as the Aluminum Bow as it contains the same
        // mesh mask as the metal arrow. This will be refactored later so that we can try and
        // save as much space as possible when it comes to custom assets.
        Material importedArrowMaterial = asset.LoadAsset<Material>("Aluminum_Bow_Material");
        // Debug.Log($"++ Updating {bowGameObject.name}...");

        ItemObjectEnabler throwablePrefabs = Traverse.Create(bowScript).Field("throwablePrefabs").GetValue() as ItemObjectEnabler;
        ItemObjectEnabler throwableModelEnabler = Traverse.Create(bowScript).Field("throwableModelEnabler").GetValue() as ItemObjectEnabler;

        // Update StringMiddle with a copy of the metal arrow model, rename it, set it to inactive, store the reference.
        Transform bowStringMiddleTransform = bowGameObject.transform.Find("bow animator/Armature_Bow/Root/StringMiddle");
        GameObject copiedArrowModelReference = null;
        foreach (Transform modelTransform in bowStringMiddleTransform) {
          if (modelTransform.gameObject.name.Contains("Model_Metal")) {
            // Checking the Metal Arrow GameObject
            // Debug.Log($"Found the Metal Arrow Model within {bowGameObject.name}'s StringMiddle");
            copiedArrowModelReference = GameObject.Instantiate(modelTransform.gameObject, bowStringMiddleTransform);
            copiedArrowModelReference.SetActive(false);
            copiedArrowModelReference.name = itemBase.UniqueName + "_Model"; // "Bow_ArrowModel_Titanium"; // Make this dynamic later.
            MeshRenderer copiedArrowModelMeshRenderer = copiedArrowModelReference.GetComponent<MeshRenderer>();
            copiedArrowModelMeshRenderer.material = importedArrowMaterial;
            // Debug.Log($"New Arrow Position for {bowGameObject.name}: " + copiedArrowModelReference.transform.localPosition);
            // Debug.Log($"Instantiated a Arrow Model Reference ({copiedArrowModelReference.name}) from {bowGameObject.name}'s StringMiddle");
            break;
          }
        }

        // Create the new arrow game object using an existing one (if it hasn't already been created).
        if (newArrowPrefab == null) {
          // Debug.Log("-- No arrow prefab was already present. Creating a cached one for now.");
          ItemModelConnection[] itemModelConnections = throwablePrefabs.GetObjectConnections();
          foreach (ItemModelConnection itemModelConnection in itemModelConnections) {
            if (itemModelConnection.item.UniqueName.Equals("Arrow_Metal")) {
              newArrowPrefab = GameObject.Instantiate(itemModelConnection.model);
              newArrowPrefab.SetActive(false);
              newArrowPrefab.name = itemBase.UniqueName;

              // We must also update the YieldHandler and PickupItem scripts to cause the items to be lootable versions of the custom arrows.
              PickupItem prefabPickupItem = newArrowPrefab.GetComponent<PickupItem>();
              prefabPickupItem.pickupTerm = itemBase.settings_Inventory.DisplayName;
              YieldHandler prefabYieldHandler = prefabPickupItem.yieldHandler;
              SO_ItemYield customItemYield = ScriptableObject.CreateInstance<SO_ItemYield>();
              Cost customCost = new Cost(itemBase, 1);
              customItemYield.yieldAssets = new List<Cost>();
              customItemYield.yieldAssets.Add(customCost);
              prefabYieldHandler.yieldAsset = customItemYield;

              // Then, update the MeshFilter's mesh to use a custom asset for the arrows.
              foreach (Transform arrowTransform in newArrowPrefab.transform) {
                if (arrowTransform.gameObject.name.Contains("model")) {
                  MeshRenderer arrowMeshRenderer = arrowTransform.gameObject.GetComponent<MeshRenderer>();
                  arrowMeshRenderer.material = importedArrowMaterial;
                  break;
                }
              }

              // Lastly, set the Arrow's script with our new modifiers.
              Arrow arrowScript = newArrowPrefab.GetComponent<Arrow>();
              Traverse.Create(arrowScript).Field("damage").SetValue(AugmentedEquipmentItems.Get(itemBase.UniqueName).modifiers["arrowDamage"]);
            }
          }
          // Debug.Log($"New arrow prefab was created -- {newArrowPrefab.name}");
        }

        // Update throwablePrefabs with the new arrow game object.
        ItemModelConnection[] prefabItemModelConnections = throwablePrefabs.GetObjectConnections();
        // Debug.Log($"Preparing to update throwablePrefab's Item Connections Array -- Current Length: {prefabItemModelConnections.Length}");
        Array.Resize(ref prefabItemModelConnections, prefabItemModelConnections.Length + 1);
        ItemModelConnection prefabItemModelConnection = new ItemModelConnection();
        prefabItemModelConnection.item = itemBase;
        prefabItemModelConnection.model = newArrowPrefab;
        prefabItemModelConnections[prefabItemModelConnections.Length - 1] = prefabItemModelConnection;
        Traverse.Create(throwablePrefabs).Field("itemConnections").SetValue(prefabItemModelConnections);
        // Debug.Log($"Completed updating throwablePrefab's Item Connections Array -- Current Length: {prefabItemModelConnections.Length}");
        // Debug.Log($"Info on the Last Object in Prefab Connections | {prefabItemModelConnections[prefabItemModelConnections.Length - 1].item.UniqueName} | {prefabItemModelConnections[prefabItemModelConnections.Length - 1].model.name}");

        // Update throwableModelEnabler with the reference to the copy of the metal arrow model.
        ItemModelConnection[] modelItemModelConnections = throwableModelEnabler.GetObjectConnections();
        // Debug.Log($"Preparing to update throwableModelEnabler's Item Connections Array -- Current Length: {modelItemModelConnections.Length}");
        Array.Resize(ref modelItemModelConnections, modelItemModelConnections.Length + 1);
        ItemModelConnection modelItemModelConnection = new ItemModelConnection();
        modelItemModelConnection.item = itemBase;
        modelItemModelConnection.model = copiedArrowModelReference;
        modelItemModelConnections[modelItemModelConnections.Length - 1] = modelItemModelConnection;
        Traverse.Create(throwableModelEnabler).Field("itemConnections").SetValue(modelItemModelConnections);
        // Debug.Log($"Completed updating throwablePrefab's Item Connections Array -- Current Length: {modelItemModelConnections.Length}");
        // Debug.Log($"Info on the Last Object in Model Connections | {modelItemModelConnections[modelItemModelConnections.Length - 1].item.UniqueName} | {modelItemModelConnections[modelItemModelConnections.Length - 1].model.name}");
        // Debug.Log("---");
      }
    }
  }
}
