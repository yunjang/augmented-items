using System.Collections;
using System.Collections.Generic;
using HarmonyLib;
using static AugmentedEquipmentItems;
using UnityEngine;

public class CustomAxe : CustomItem {
  public CustomAxe(Transform playerRightHandTransform, Dictionary<string, ItemConnection> connectionDictionary, Dictionary<string, ItemConnection> addedConnectionDictionary, List<AnimationConnection> animationConnections, List<AnimationConnection> addedAnimationConnectionList, AssetBundle asset) : base(playerRightHandTransform, connectionDictionary, addedConnectionDictionary, animationConnections, addedAnimationConnectionList, asset) { }

  public void Create(Item_Base itemBase) {
    GameObject axeTemplateGameObject = null;
    foreach (Transform rightHandItem in base.playerRightHandTransform) {
      Axe templateAxeScript = rightHandItem.gameObject.GetComponent<Axe>();
      if (templateAxeScript != null) {
        axeTemplateGameObject = rightHandItem.gameObject;
        break;
      }
    }

    AnimationConnection hitAnimation = null;
    AnimationConnection startAnimation = null;
    foreach (AnimationConnection connection in base.animationConnections) {
      if (connection.name.Equals("Hammer_Hit | OnAxeHit") && hitAnimation == null) {
        hitAnimation = new AnimationConnection();
        base.CopyAnimationConnection(hitAnimation, connection);
      }
      if (connection.name.Equals("Hammer_Hit | OnAxeStart") && startAnimation == null) {
        startAnimation = new AnimationConnection();
        base.CopyAnimationConnection(startAnimation, connection);
      }
    }

    GameObject newAxeGameObject = GameObject.Instantiate(axeTemplateGameObject, base.playerRightHandTransform);
    newAxeGameObject.SetActive(false);
    newAxeGameObject.name = itemBase.UniqueName;
    newAxeGameObject.transform.localPosition = Vector3.zero;

    // Custom modifiers go here for the axe.
    Traverse.Create(itemBase.settings_usable).Field("useButtonCooldown").SetValue(AugmentedEquipmentItems.Get(itemBase.UniqueName).modifiers["useButtonCooldown"]);

    // The axe object itself contains the MeshRenderer, so no need to navigate the GameObject.
    Material importedAxeMaterial = asset.LoadAsset<Material>(itemBase.UniqueName + "_Material");
    MeshRenderer axeMeshRenderer = newAxeGameObject.GetComponent<MeshRenderer>();
    axeMeshRenderer.material = importedAxeMaterial;

    ItemConnection itemConnection = new ItemConnection();
    itemConnection.name = itemBase.UniqueName;
    itemConnection.inventoryItem = itemBase;
    itemConnection.obj = newAxeGameObject;
    connectionDictionary.TryAdd(itemBase.UniqueName, itemConnection);
    addedConnectionDictionary.TryAdd(itemBase.UniqueName, itemConnection);

    hitAnimation.objectReciever = newAxeGameObject;
    animationConnections.Add(hitAnimation);
    addedAnimationConnectionList.Add(hitAnimation);

    startAnimation.objectReciever = newAxeGameObject;
    animationConnections.Add(startAnimation);
    addedAnimationConnectionList.Add(startAnimation);
  }
}
