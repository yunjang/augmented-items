using System.Collections;
using System.Collections.Generic;

public class ItemTemplate {
  public string baseItemReference;
  public string uniqueName;
  public string displayName;
  public int uniqueIndex;
  public string description;
  public int maxUses;
  public EquipSlotType equipSlotType;
  public CraftingCategory craftingCategory;
  public string subCategory;
  public int subCategoryOrder;
  public Dictionary<string, int> recipe;
  public Dictionary<string, float> modifiers;
  public string spriteName;

  public ItemTemplate(string baseItemReference, string uniqueName, string displayName, int uniqueIndex, string description, int maxUses, EquipSlotType equipSlotType, CraftingCategory craftingCategory, string subCategory, int subCategoryOrder, Dictionary<string, int> recipe, Dictionary<string, float> modifiers, string spriteName = null) {
    this.baseItemReference = baseItemReference;
    this.uniqueName = uniqueName;
    this.displayName = displayName;
    this.uniqueIndex = uniqueIndex;
    this.description = description;
    this.maxUses = maxUses;
    this.equipSlotType = equipSlotType;
    this.craftingCategory = craftingCategory;
    this.subCategory = subCategory;
    this.subCategoryOrder = subCategoryOrder;
    this.recipe = recipe;
    this.modifiers = modifiers;
    this.spriteName = spriteName;
  }
}

public static class AugmentedEquipmentItems {
  public static Dictionary<string, ItemTemplate> AllAugmentedEquipmentItems = new Dictionary<string, ItemTemplate> {
    ["Hook_Titanium"] = new ItemTemplate(
      "Hook_Plastic",
      "Hook_Titanium",
      "Titanium Hook",
      16600,
      "An improved hook using titanium. The heavier weight makes it harder to throw but much more durable and better at pulling items out of the ground.",
      500,
      EquipSlotType.None,
      CraftingCategory.Tools,
      "Hooks",
      2,
      new Dictionary<string, int> { ["TitaniumIngot"] = 6, ["Bolt"] = 2, ["Scrap"] = 10, ["Rope"] = 4 },
      new Dictionary<string, float> {
        ["gatherTime"] = 0.5f
      }
    ),
    ["Titanium_Shovel"] = new ItemTemplate(
      "Shovel",
      "Titanium_Shovel",
      "Titanium Shovel",
      16601,
      "An improved shovel made out of titanium. It's more durable while being faster to handle.",
      120,
      EquipSlotType.None,
      CraftingCategory.Tools,
      "",
      1,
      new Dictionary<string, int> { ["TitaniumIngot"] = 4, ["Bolt"] = 4, ["Scrap"] = 15, ["Plank"] = 20 },
      new Dictionary<string, float> {
        ["digSpeedModifier"] = 0.5f
      }
    ),
    ["Aluminum_Bow"] = new ItemTemplate(
      "Bow",
      "Aluminum_Bow",
      "Aluminum Bow",
      16602,
      "An improved bow made out of scrap aluminum and other lesser metals. It's more durable and a little bit easier to pull back but not by much.",
      110,
      EquipSlotType.None,
      CraftingCategory.Weapons,
      "CustomBowCategory",
      0,
      new Dictionary<string, int> { ["Bolt"] = 2, ["Hinge"] = 2, ["Scrap"] = 6, ["Plank"] = 8, ["Rope"] = 4, ["VineGoo"] = 2 },
      new Dictionary<string, float> {
        ["chargeSpeedModifier"] = 0.4f
      }
    ),
    ["Titanium_Arrow"] = new ItemTemplate(
      "Arrow_Metal",
      "Titanium_Arrow",
      "Titanium Arrow",
      16603,
      "Razor sharp arrows made from titanium.",
      1,
      EquipSlotType.None,
      CraftingCategory.Weapons,
      "CustomBowCategory",
      1,
      new Dictionary<string, int> { ["TitaniumIngot"] = 1, ["Plank"] = 3, ["Feather"] = 6 },
      new Dictionary<string, float> {
        ["arrowDamage"] = 20f
      }
    ),
    ["Placeable_Storage_MediumTitanium"] = new ItemTemplate(
      "Placeable_Storage_Medium",
      "Placeable_Storage_MediumTitanium",
      "Titanium Storage",
      16604,
      "You're not really sure how. But you managed to take a large storage made of titanium with bits and pieces of other material to make a smaller chest that is just as large. Raft-engineering at its finest.",
      1,
      EquipSlotType.None,
      CraftingCategory.Other,
      "Storages",
      3,
      new Dictionary<string, int> { ["Placeable_Storage_Large"] = 1, ["CopperIngot"] = 5, ["MetalIngot"] = 5, ["Plastic"] = 30, ["Nail"] = 40 },
      null
    ),
    ["Placeable_MotorWheel_Titanium"] = new ItemTemplate(
      "Placeable_MotorWheel",
      "Placeable_MotorWheel_Titanium",
      "Titanium Engine",
      16605,
      "Using more refined material along with additional properties to improve on its combustion design, this upgraded engine provides your raft with greater knots per second while improving on fuel efficiency.",
      1,
      EquipSlotType.None,
      CraftingCategory.Navigation,
      "",
      1,
      new Dictionary<string, int> { ["Placeable_MotorWheel"] = 1, ["TitaniumIngot"] = 20, ["ExplosivePowder"] = 30, ["MetalIngot"] = 20, ["CopperIngot"] = 20, ["BioFuel"] = 10 },
      new Dictionary<string, float> {
        ["_raftSpeed"] = 3.5f,
        ["timePerFuel"] = 18f,
        ["wheelRotationSpeed"] = 105f,
        ["motorStrength"] = 200f
      }
    ),
    ["Placeable_CookingStand_SmelterArc"] = new ItemTemplate(
      "Placeable_CookingStand_Smelter",
      "Placeable_CookingStand_SmelterArc",
      "Arc Furnace",
      16606,
      "Using some scrap electronics and titanium, you were able to create a crucible-like smelter that can reach its peak temperature more quickly.",
      1,
      EquipSlotType.None,
      CraftingCategory.Other,
      "CustomCookingStand",
      0,
      new Dictionary<string, int> { ["Placeable_CookingStand_Smelter"] = 1, ["TitaniumIngot"] = 5, ["ExplosivePowder"] = 5, ["Battery"] = 3, ["CircuitBoard"] = 6, ["Brick_Dry"] = 10 },
      new Dictionary<string, float> {
        ["cookTimeMultiplier"] = 0.5f
      }
    ),
    ["Titanium_Axe"] = new ItemTemplate(
      "Axe",
      "Titanium_Axe",
      "Titanium Axe",
      16607,
      "An improved axe made of titanium. Not only is it more durable, it's lighter in the hands when swinging.",
      300,
      EquipSlotType.None,
      CraftingCategory.Tools,
      "CustomAxeCategory",
      0,
      new Dictionary<string, int> { ["TitaniumIngot"] = 4, ["Bolt"] = 2, ["Scrap"] = 10, ["Plank"] = 10, ["VineGoo"] = 2 },
      new Dictionary<string, float> {
        ["useButtonCooldown"] = 0.605f
      }
    )
  };

  public static ItemTemplate Get(string uniqueName) {
    return AllAugmentedEquipmentItems[uniqueName];
  }
}