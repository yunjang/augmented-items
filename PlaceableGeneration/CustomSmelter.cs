using System.Collections;
using System.Collections.Generic;
using static AugmentedEquipmentItems;
using HarmonyLib;
using UnityEngine;

public class CustomSmelter : CustomPlaceable {
  public CustomSmelter(AssetBundle asset) : base(asset) { }

  public void Create(Item_Base itemBase) {
    ItemInstance_Buildable buildableInstance = itemBase.settings_buildable;
    Block cookingStand = buildableInstance.GetBlockPrefab(0);

    Block newCookingStand = UnityEngine.Object.Instantiate(cookingStand);
    newCookingStand.buildableItem = itemBase;
    newCookingStand.itemToReturnOnDestroy = itemBase;
    newCookingStand.gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
    newCookingStand.gameObject.transform.position = new Vector3(0f, 5000f, 0f);
    UnityEngine.Object.DontDestroyOnLoad(newCookingStand.gameObject);

    // Update the asset here for the new smelter block.
    Transform cookingStandTransform = newCookingStand.gameObject.transform.Find("model");
    Material customMaterial = asset.LoadAsset<Material>(itemBase.UniqueName + "_Material");
    MeshRenderer meshRenderer = cookingStandTransform.gameObject.GetComponent<MeshRenderer>();
    meshRenderer.material = customMaterial;

    // Go through the Cooking Stand's transform and update the Cooking Slot's properties.
    Dictionary<string, float> cookingStandModifiers = AugmentedEquipmentItems.Get(itemBase.UniqueName).modifiers;
    foreach (Transform cookingSlotTransform in newCookingStand.gameObject.transform) {
      CookingSlot cookingSlotScript = cookingSlotTransform.gameObject.GetComponent<CookingSlot>();
      if (cookingSlotScript != null) {
        Traverse.Create(cookingSlotScript).Field("cookTimeMultiplier").SetValue(cookingStandModifiers["cookTimeMultiplier"]);
      }
    }

    // Create a brand new ItemInstance_Buildable, assign the cloned object, its properties from the original block, and save it to the block prefabs.
    ItemInstance_Buildable newBuildableInstance = new ItemInstance_Buildable(newCookingStand, buildableInstance.ReselectOnBuild, buildableInstance.Placeable, buildableInstance.HideWhenOverlapSamePos);
    newBuildableInstance.PlayParticlesWhenRemoved = buildableInstance.PlayParticlesWhenRemoved;
    newBuildableInstance.ReturnItemWhenRemovedRecursively = buildableInstance.ReturnItemWhenRemovedRecursively;
    itemBase.settings_buildable = newBuildableInstance;

    // Debug.Log("MotorWheel Code: " + newMotorWheelBlock);
    RAPI.AddItemToBlockQuadType(itemBase, RBlockQuadType.quad_foundation);
    RAPI.AddItemToBlockQuadType(itemBase, RBlockQuadType.quad_floor);
  }
}
