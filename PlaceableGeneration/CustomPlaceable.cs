using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomPlaceable {
  public AssetBundle asset;

  public CustomPlaceable(AssetBundle asset) {
    this.asset = asset;
  }
}