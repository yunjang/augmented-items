using System.Collections;
using System.Collections.Generic;
using static AugmentedEquipmentItems;
using HarmonyLib;
using UnityEngine;

public class CustomEngine : CustomPlaceable {
  public CustomEngine(AssetBundle asset) : base(asset) { }

  public void Create(Item_Base itemBase) {
    ItemInstance_Buildable buildableInstance = itemBase.settings_buildable;
    Block motorWheelBlock = buildableInstance.GetBlockPrefab(0);

    Block newMotorWheelBlock = UnityEngine.Object.Instantiate(motorWheelBlock);
    newMotorWheelBlock.buildableItem = itemBase;
    newMotorWheelBlock.itemToReturnOnDestroy = itemBase;
    newMotorWheelBlock.gameObject.transform.position = new Vector3(0f, 5000f, 0f);
    UnityEngine.Object.DontDestroyOnLoad(newMotorWheelBlock.gameObject);

    // Update the asset here for the new motor wheel block.
    Transform engineTransform = newMotorWheelBlock.gameObject.transform.Find("Parent/Engine/Engine");
    Material customEngineMaterial = asset.LoadAsset<Material>(itemBase.UniqueName + "_Material");
    SkinnedMeshRenderer skinnedMeshRenderer = engineTransform.gameObject.GetComponent<SkinnedMeshRenderer>();
    Material[] customMaterialSet = new Material[2];
    customMaterialSet[0] = customEngineMaterial;
    customMaterialSet[1] = skinnedMeshRenderer.materials[1];
    skinnedMeshRenderer.materials = customMaterialSet;

    MotorWheel motorWheelScript = newMotorWheelBlock.gameObject.GetComponent<MotorWheel>();
    Dictionary<string, float> customMotorWheelAugments = AugmentedEquipmentItems.Get(itemBase.UniqueName).modifiers;
    Traverse.Create(motorWheelScript).Field("_raftSpeed").SetValue(customMotorWheelAugments["_raftSpeed"]);
    Traverse.Create(motorWheelScript).Field("timePerFuel").SetValue(customMotorWheelAugments["timePerFuel"]);
    Traverse.Create(motorWheelScript).Field("wheelRotationSpeed").SetValue(customMotorWheelAugments["wheelRotationSpeed"]);
    Traverse.Create(motorWheelScript).Field("_motorStrenght").SetValue((int) customMotorWheelAugments["motorStrength"]);

    // Create a brand new ItemInstance_Buildable, assign the cloned object, its properties from the original block, and save it to the block prefabs.
    ItemInstance_Buildable newBuildableInstance = new ItemInstance_Buildable(newMotorWheelBlock, buildableInstance.ReselectOnBuild, buildableInstance.Placeable, buildableInstance.HideWhenOverlapSamePos);
    newBuildableInstance.PlayParticlesWhenRemoved = buildableInstance.PlayParticlesWhenRemoved;
    newBuildableInstance.ReturnItemWhenRemovedRecursively = buildableInstance.ReturnItemWhenRemovedRecursively;
    itemBase.settings_buildable = newBuildableInstance;

    // Debug.Log("MotorWheel Code: " + newMotorWheelBlock);
    RAPI.AddItemToBlockQuadType(itemBase, RBlockQuadType.quad_foundation_empty);
  }
}
