using System.Collections;
using System.Collections.Generic;
using static AugmentedEquipmentItems;
using HarmonyLib;
using UnityEngine;

public class CustomChest : CustomPlaceable {
  public CustomChest(AssetBundle asset) : base(asset) { }

  // ItemInstance_Buildable buildableInstance = itemBase.settings_buildable;
  // Debug.Log("++ CustomChest Start");
  // Debug.Log("Item Base: " + itemBase.UniqueName);

  // Grab the respective chest block piece and then nab the inventory prefab that is tied to it.
  // Storage_Small referenceChestFloor = buildableInstance.GetBlockPrefab(0) as Storage_Small;
  // Storage_Small referenceChestWall = buildableInstance.GetBlockPrefab(1) as Storage_Small;
  // Inventory referenceInventoryPrefab = referenceChestFloor.inventoryPrefab;

  // GameObject customChestFloor = GameObject.Instantiate(referenceChestFloor.gameObject);
  // customChestFloor.SetActive(false);
  // GameObject customChestWall = GameObject.Instantiate(referenceChestWall.gameObject);
  // customChestWall.SetActive(false);
  // GameObject customInventoryPrefab = GameObject.Instantiate(referenceInventoryPrefab.gameObject);
  // customInventoryPrefab.SetActive(false);

  // Block customChestFloorBlockScript = customChestFloor.GetComponent<Block>();
  // customChestFloorBlockScript.buildableItem = itemBase;
  // customChestFloorBlockScript.itemToReturnOnDestroy = itemBase;
  // Block customChestWallBlockScript = customChestWall.GetComponent<Block>();
  // customChestWallBlockScript.buildableItem = itemBase;
  // customChestWallBlockScript.itemToReturnOnDestroy = itemBase;

  // Block[] customBlockPrefabs = new Block[2];
  // referenceChestFloor = (customChestFloorBlockScript as Storage_Small);
  // referenceChestWall = (customChestWallBlockScript as Storage_Small);
  // customBlockPrefabs[0] = referenceChestFloor;
  // customBlockPrefabs[1] = referenceChestWall;
  // Debug.Log($"One: {customBlockPrefabs[0]} | Two: {customBlockPrefabs[1]}");
  // Debug.Log($"One HP: {customBlockPrefabs[0].Health} | Two Health: {customBlockPrefabs[1].Health}");
  // Traverse.Create(buildableInstance).Field("blockPrefabs").SetValue(customBlockPrefabs);

  // foreach (Transform modelTransform in customChestFloor.gameObject.transform) {
  //   MeshRenderer meshRenderer = null;
  //   if (modelTransform.gameObject.name.Contains("_Bottom") || modelTransform.gameObject.name.Contains("_Top")) {
  //     meshRenderer = modelTransform.gameObject.GetComponent<MeshRenderer>();
  //     meshRenderer.material = asset.LoadAsset<Material>("Placeable_StorageMedium_Titanium");
  //   }
  // }

  // foreach (Transform modelTransform in customChestWall.gameObject.transform) {
  //   MeshRenderer meshRenderer = null;
  //   if (modelTransform.gameObject.name.Contains("_Bottom") || modelTransform.gameObject.name.Contains("_Top")) {
  //     meshRenderer = modelTransform.gameObject.GetComponent<MeshRenderer>();
  //     meshRenderer.material = asset.LoadAsset<Material>("Placeable_StorageMedium_Titanium");
  //   }
  // }

  // referenceChestFloor = customChestFloor;

  // GameObject customChestFloor = GameObject.Instantiate(referenceChestFloor.gameObject);
  // GameObject customChestWall = GameObject.Instantiate(referenceChestWall.gameObject);

  // Storage_Small storageScript = customChestFloor.GetComponent<Storage_Small>();
  // Debug.Log("Script HP: " + storageScript.Health);

  // Block[] customBlockPrefabs = new Block[2];
  // customBlockPrefabs[0] = customChestFloor.GetComponent<Block>();
  // customBlockPrefabs[1] = customChestWall.GetComponent<Block>();

  // Traverse.Create(buildableInstance).Field("blockPrefabs").SetValue(customBlockPrefabs);

  // Storage_Small storageScript = customChestFloor.GetComponent<Storage_Small>();
  // Debug.Log("Storage Script HP: " + storageScript.Health);
  // GameObject bowTemplateGameObject = null;
  // foreach (Transform rightHandItem in base.playerRightHandTransform) {
  //   ThrowableComponent_Bow templateBowScript = rightHandItem.gameObject.GetComponent<ThrowableComponent_Bow>();
  //   if (templateBowScript != null) {
  //     bowTemplateGameObject = rightHandItem.gameObject;
  //     break;
  //   }
  // }

  // GameObject newBowGameObject = GameObject.Instantiate(bowTemplateGameObject, base.playerRightHandTransform);
  // newBowGameObject.SetActive(false);
  // newBowGameObject.name = itemBase.UniqueName;
  // newBowGameObject.transform.localPosition = Vector3.zero;

  // ChargeMeter bowChargeMeter = newBowGameObject.GetComponent<ChargeMeter>();
  // Traverse.Create(bowChargeMeter).Field("chargeSpeed").SetValue(75f + (75f * AugmentedEquipmentItems.Get(itemBase.UniqueName).modifiers["chargeSpeedModifier"]));

  // foreach (Transform bowTransform in newBowGameObject.transform) {
  //   if (bowTransform.gameObject.name.Equals("bow animator")) {
  //     foreach (Transform bowModelTransform in bowTransform) {
  //       if (bowModelTransform.gameObject.name.Contains("model")) {
  //         GameObject bowModelObject = bowModelTransform.gameObject;
  //         Material importedBowMaterial = asset.LoadAsset<Material>(itemBase.UniqueName + "_Material");
  //         SkinnedMeshRenderer bowMeshRenderer = bowModelObject.GetComponent<SkinnedMeshRenderer>();
  //         bowMeshRenderer.material = importedBowMaterial;
  //       }
  //     }
  //   }
  // }

  // RAPI.AddItemToBlockQuadType(itemBase, RBlockQuadType.quad_floor);
  // RAPI.AddItemToBlockQuadType(itemBase, RBlockQuadType.quad_foundation);
  // RAPI.AddItemToBlockQuadType(itemBase, RBlockQuadType.quad_wall);

  // ItemConnection floorItemConnection = new ItemConnection();
  // floorItemConnection.name = itemBase.UniqueName;
  // floorItemConnection.inventoryItem = itemBase;
  // floorItemConnection.obj = customChestFloor;
  // connectionDictionary.TryAdd(itemBase.UniqueName, floorItemConnection);
  // addedConnectionDictionary.TryAdd(itemBase.UniqueName, floorItemConnection);

  // ItemConnection wallItemConnection = new ItemConnection();
  // wallItemConnection.name = itemBase.UniqueName;
  // wallItemConnection.inventoryItem = itemBase;
  // wallItemConnection.obj = customChestWall;
  // connectionDictionary.TryAdd(itemBase.UniqueName, wallItemConnection);
  // addedConnectionDictionary.TryAdd(itemBase.UniqueName, wallItemConnection);
}
